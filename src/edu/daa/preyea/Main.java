package edu.daa.preyea;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.*;

public class Main {

    //Entry point for our console application.
    public static void main(String[] args) throws Exception {

         //Create time stamp when this method is invoked from console.
        final long startTime = System.nanoTime();

        int[][] adjMatrix;

        //Console input stream reader to capture input data.
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter filename.");
        String fileName = reader.readLine();

        System.out.println("Enter no of nodes.");
        int nNodes = Integer.parseInt(reader.readLine());

        //Load file data as adjacency matrix
        adjMatrix = createAdjMatrixFromFile(nNodes, fileName);

        System.out.println("Enter the source node ID.");
        int sID = Integer.parseInt(reader.readLine());

        //Close the console input stream reader.
        reader.close();

        //If the sID is not within the range, we throw exception
        if(sID<1||sID>nNodes)
            throw new IllegalArgumentException("Invalid sID range");

        performBFS(sID, nNodes, adjMatrix);

        //Calculate the all-time after everything is computed
        final long allTime=(System.nanoTime() - startTime)/ 1000000;

        System.out.println("\nall_time : "+allTime+" milliseconds");

    }

    //For the calculation part, we are using array with start index value 0.
    //But for displaying purpose we are considering array with start index value 1.
    //Read the data from file. If the file is invalid, we throw exception
    private static int[][] createAdjMatrixFromFile(int noOfNodes, String filePath) throws Exception {
        //We are trying to read the file line by line
        BufferedReader br = new BufferedReader(new FileReader(filePath));

        int[][] adjMatrix = new int[noOfNodes][noOfNodes];
        for (int i = 0; i <noOfNodes; i++) {
            //For each read line from the file, we try to split the data into array of string
            //seperated by whitespace.
            String[] rowData = br.readLine().split(" ");
            for (int j = 0; j <noOfNodes; j++) {
                //Add each data to corresponding cell value of matrix
                adjMatrix[i][j] = Integer.parseInt(rowData[j]);
            }
        }
        //Close the stream.
        br.close();
        return adjMatrix;
    }


    static void performBFS(int start, int v, int[][] adjMatrxi)
    {
        //We are processing array from index 0 on calculation part
        // and for the presentation part,
        //we are considering the array to start from 1;
        start--;
        boolean[] visited = new boolean[v];
        Arrays.fill(visited, false);
        //Array to hold nodes in current layer
        List<Integer> currentLayer;
        //Array to hold nodes in next layer
        List<Integer> nextLayer = new ArrayList<>();

        //We are using the user provided node as starting node.
        nextLayer.add(start);
        visited[start]=true;
        int layerLevel=0;

        //Since we add the nodes that are yet to be traversed in 'nextLayer' array.
        //We try to find the adjacent nodes till we don't have any
        // nodes to be traversed.
        while (!nextLayer.isEmpty())
        {


            //We assign the nextLayer values to current layer so that we can obtain
            // their adjacent nodes
            //and update back to nextLayer.
            currentLayer=nextLayer;

            //Print the current layer on the console.
            printLayer(layerLevel,currentLayer);

            //At this point we try to find the adjacent nodes in next layer.
            nextLayer=new ArrayList<>();
            for (int currentNode : currentLayer) {
                for (int i = 0; i < v; i++) {
                    //Checking if there is any edge from the current selected node
                    //to remaining nodes in our adjacency matrix representation of our graph
                    //and if those node are yet be visisted, we add those node
                    //in next layer and mark the node as visited.
                    if (adjMatrxi[currentNode][i] == 1 && !visited[i]) {
                        nextLayer.add(i);
                        visited[i] = true;
                    }
                }
            }
            //Update the layer index
            layerLevel++;
        }
    }

    static void printLayer(int layerIndex, List<Integer> layerNodes)
    {
        System.out.println();
        System.out.print("L"+layerIndex+": ");
        for(int node:layerNodes)
        {
            //For the calculation part, we are using array with start index value 0.
            //But for displaying purpose we are considering array with start index value 1.
            System.out.print(node+1+" ");
        }
    }


}
